# Computing Projects #
These projects are hosted at: https://34993230.bitbucket.io/  
Here are the projects that are included within this repository:

  
## Computing Project Portal [Stable] ## 
This project is a website that can redirect you to the various projects that are stored within this repository.  
Last Modified: 28/08/19  
© 2019  
Development State: **Finished!**  

## Computing Project Portal V2 [Stable] ## 
This project is was built to replace old portal, better interface and interactive elements.  
Last Modified: 19/09/19  
© 2019  
Development State: **Finished!**

## MathsMan Project [Stable] ##  
MathsMan is a basic calculator that can calculate: Addition, Subtraction, Multiplication and Division
equations. Calculator also has validation and if the answer is a decimal, it will round it to 2 decimal places.  
Last Modified: 16/08/19  
© 2019  
Development State: **Finished!**  

## FortnightlyConvert Project [Stable] ##  
This application allows the user to calculate their Yearly wage by entering what they earn on a fortnightly basis.  
Last Modified: 26/08/19  
© 2019  
Development State: **Finished!**  

## VolumeConvert Project [Stable] ##  
This project allows you to calculate the volume of various shapes.  
Last Modified: 29/08/19  
© 2019  
Development State: **Finished!**  

## CommissionConvert Project [Stable] ##  
CommissionConvert allows you to enter the value of the property and it will identify the return and commission value.  
Last Modified: 4/09/19  
© 2019  
Development State: **Finished!**  

## ShareCalculator Project [Stable] ##  
Share calculator allows you to select a company and quantity then it will calculate the total cost. (share price is fictional)  
Last Modified: 10/09/19  
© 2019  
Development State: **Finished!**  

## PetroleumCalculator Project [Stable] ##  
Allows the user to calculate the bonus and litres sold over two fortnightly periods.  
Last Modified: 16/09/19  
© 2019  
Development State: **Finished!** 